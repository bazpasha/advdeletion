import advdeletion as ad
import pickle

# Loading pickled model
with open('model.pkl', 'r') as model_file:
    model = pickle.load(model_file)

# Loading database - list of StipToSequence
with open('database.pkl', 'r') as database:
    db = pickle.load(database)

# Loading video
full = ad.AdvDeletion('full_video.mp4', model)

# Searching adverts
intervals = []
for adv in db:
    intervals.extend(full.deleteAdv(adv))
intervals = ad.covering(intervals)

# Erasing adverts
cutter = ad.VideoCutter('full_video.mp4')
cutter.erase(intervals, 'clear_video.mp4')