#[Анализ видеопоследовательностей](http://wiki.cs.hse.ru/%D0%90%D0%BD%D0%B0%D0%BB%D0%B8%D0%B7_%D0%B2%D0%B8%D0%B4%D0%B5%D0%BE%D0%BF%D0%BE%D1%81%D0%BB%D0%B5%D0%B4%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D0%B5%D0%B9_%D0%B4%D0%BB%D1%8F_%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%BE%D0%B3%D0%BE_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA%D0%B0_%D0%B8_%D0%B2%D1%8B%D0%B4%D0%B5%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F_%D1%80%D0%B5%D0%BA%D0%BB%D0%B0%D0%BC%D1%8B)#
*Хрушков Павел, БПМИ156*

Целью проекта является создание инструмента для удаления рекламного блока из видео. Используемые в проекте технологии могут быть обобщены для достижения других целей: поиск подпоследовательсностей в видео, обнаружение особых точек и пр.

Анализ изображений и видео — наверное, самая сложная область машинного обучения: за все эти годы был проделан относительно небольшой прогресс. В то время как, скажем, диалоговые системы близки к прохождению теста Тьюринга, 
программы по распознаванию изображений вряд ли могут похвастаться такими же результатами. И именно поэтому любое исследование в области анализа изображений и видео сейчас актуально. Основой проекта стали исследования [Ивана Лаптева](https://www.di.ens.fr/~laptev/interestpoints.html).

В проекте использованы следующие инструменты:

* **OpenCV** как библиотека для работы с изображениями и видео
* **STIP** — библиотека для обнаружения особых точек видео
* **scikit-learn** — библиотека для работы с машинным обучением

В результате работы над проектом была разработана небольшая библиотека `advdeletion`. Теоретические обоснования применямых методов можно изучить в небольшом отчете Report.pdf. 

##Глобальные переменные##
* `STIP_FOLDER` - путь до папки с библиотекой STIP. По умолчанию `STIP_FOLDER = ''`;
* `STIPPED_VIDEOS_FOLDER` - папка, в которую будут сохраняться сжатые видео для последующей обработки STIP. По умолчанию `STIPPED_VIDEOS_FOLDER = 'stipped_videos'`;
* `VIDEOCUTTER_FOLDER` - папка, в которой будут храниться временные файлы класса `VideoCutter`. По умолчанию `VIDEOCUTTER_FOLDER = 'videocutter'`;
* `COMPRESS_SIZE` - размер, до которого будет сжиматься видео, чтобы быстрее обрабатываться STIP. По умолчанию `COMPRESS_SIZE = (200, 200)`.

##Класс `FrameToCluster`##
Класс является оболочкой алгоритма KMeans из библиотеки sklearn. Разница заключается в том, что класс хранит частоты появлений кластеров и имеет некоторые дополнительные методы.

###Конструктор###
Принимает одно значение: количество кластеров.

###Метод `fit()`###
Получает на вход матрицу, строки которой - векторы для кластеризации, и производит кластеризацию. Во время работы этого метода обновляется вектор частот появлений кластеров.

###Метод `predict()`###
Получает на вход матрицу, строки которой - векторы, кластеры которых необходимо узнать. Возвращает вектор предсказанных кластеров.

###Метод `get_cluster()`###
Получает на вход матрицу, строки которой - векторы для данного кадра. Возвращает единственный кластер, соответствующий данному кадру. О том, как работает этот метод, читайте в отчете.

###Пример###
```python
import advdeletion as ad
import numpy as np
X = np.matrix([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
Y = np.matrix([[1, 5, 9]])
model = ad.FrameToCluster(2)
model.fit(X)
print(model.predict(X))
print(model.get_cluster(Y))
```

##Класс `StipToSequence`##
Преобразует матрицу, полученную в результате работы STIP (см. `AdvDeletion.read_stip()`), в последовательность кластеров. Предоставляет методы для работы с последовательностями. Во всех методах библиотеки время нормировано, т.е. представляется числом от 0 до 1.

###Конструктор###
Получает на вход матрицу, полученную в результате работы STIP, и обученную модель `FrameToCluster`. Конвертирует матрицу в последовательность чисел (кластеров).

###Метод `find_similar()`###
Реализует алгоритм Смита-Ватермана (подробнее читайте в отчете). Принимает на вход вторую последовательность (имеющую тип `StipToSequence`) и несколько необязательных параметров: 

* `diagonal`: если установлено значение `True`, будут рассматриваться только элементы, расположенные сторого над главной диагональю матрицы алгоритма;
* `min_L`: пороговое значение коэффициента L (см. отчет)
* `min_duration`: минимальная длительность найденных роликов

###Метод `find_repeating()`###
Находит повторяющиеся фрагменты с помощью алгоритма Смита-Ватермана, дублируя вход (см. отчет). Параметры этого метода имеют тот же смысл, что и параметры метода `find_similar()`.

###Пример###
```python
import advdeletion as ad
...
sequence = ad.StipToSequence(stip_matrix, model)
print(sequence.find_repeating(min_L=0.8))
```

##Класс `AdvDeletion`##
Сжимает переданное ему видео, обрабатывает библиотекой STIP, преобразует видео в последовательность чисел (кластеров).

###Конструктор###
Принимает на вход путь до видеофайла и несколько необязательных параметров:

* `stip_path` - путь до папки с библиотекой STIP. Если не установлен, устанавливается значением `STIP_PATH`;
* `compress_size` - размер сжатия видео. Если не установлени, устанавливается значением `COMPRESS_SIZE`.

###Метод `read_stip()`###
Преобразует файл, полученный в результате работы библиотеки STIP, в матрицу формата `y_norm, x_norm, t_norm, HOG-HOF` (для большей информации см. документацию к библиотеке STIP).

###Метод `extract_adv()`###
Возвращает временные интервалы, в которые заключены повторяющиеся последовательности.

###Метод `delete_adv()`###
Принимает на вход объект типа `StipToSequence`. Возвращает временные интервалы, в которые заключена искомая последовательность.

###Примеры###
Примеры можно посмотреть в файлах `example_search.py`, `example_repeating.py`, `example_db.py`.

##Класс `VideoCutter`##
Вспомогательный класс для обработки видео.

###Конструктор###
Принимает на вход видеофайл, с которым необходимо работать.

###Метод `extract()`###
Принимает на вход временные интервалы и название папки, в которую нужно сохранить выделенные временными интервалами видеофрагменты.

###Метод `erase()`###
Принимает на вход временные интервалы и название файла, в который будет сохранено видео без переданных временных интервалов.

###Примеры###
Примеры можно посмотреть в файлах `example_search.py`, `example_repeating.py`, `example_db.py`.

##Функция covering()##
Из набора временных интервалов выделяет максимальное количество непересекающихся интервалов.

###Пример###
```python
intervals = [(0.3, 0.7), (0.2, 0.5), (0.6, 0.9)]
print(covering(intervals)) # [(0.2, 0.5), (0.6, 0.9)]
```
