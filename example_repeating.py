import advdeletion as ad
import pickle

# Loading pickled model
with open('model.pkl', 'r') as model_file:
    model = pickle.load(model_file)

# Setting STIP path
ad.STIP_FOLDER = '/home/bazpasha/Desktop/stip'

# Opening videos
full = ad.AdvDeletion('toy_repeatings.mp4', model)

print('Searching for repeating parts...')
intervals = full.extractAdv()

with open('repeating_intervals.txt', 'w') as output:
    output.write(str(intervals))