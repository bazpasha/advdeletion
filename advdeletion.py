import cv2
import numpy as np
import os
from sklearn.cluster import KMeans
from scipy.special import comb
import subprocess
import collections
import math

STIP_FOLDER = ''
STIPPED_VIDEOS_FOLDER = 'stipped_videos'
VIDEOCUTTER_FOLDER = 'videocutter'
COMPRESS_SIZE = (200, 200)

def covering(intervals):
    intervals = sorted(intervals)
    end = -1
    result = []
    for interval in intervals:
        if interval[0] >= end:
            result.append(interval)
            end = interval[1]
    return result

class FrameToCluster:
    """
    The class is based on KMeans model, but it also stores
    the distribution of clusters. Thus, it can choose the
    most unique cluster from a frame represented by
    matrix.
    """
    def __init__(self, clusters):
        """
        Initialize the KMeans model.
        :param clusters: number of clusters
        """
        self.num_clusters = clusters
        self.model = KMeans(n_clusters=clusters, n_jobs=-1)

    def fit(self, X):
        """
        Clusters the given vectors and updates clusters
        distribution.
        :param X: matrix of vectors to cluster
        """
        self.model.fit(X)
        points = self.model.labels_
        self.distr = dict(collections.Counter(points))
        total = len(points)
        for i in range(self.num_clusters):
            self.distr[i] = float(self.distr.get(i, 0)) / total

    def predict(self, X):
        """
        Predicts the clusters for each vector in X
        :param X: matrix of vectors, which clusters are
        to be predicted
        :return: clusters of these vectors
        """
        return self.model.predict(X)

    def get_cluster(self, X):
        """
        Distinguishes the most unique cluster among all
        clusters of a frame represented by X
        :param X: matrix of frame vectors
        :return: the most unique cluster
        """
        points = self.model.predict(X)
        amount = dict(collections.Counter(points))
        total = len(points)
        result, max_u = -1, 1
        for i in range(self.num_clusters):
            if i not in amount or amount[i] == 0:
                continue
            u = -math.log(comb(total, amount[i]))
            u -= amount[i] * math.log(self.distr[i])
            u -= (total - amount[i]) * math.log(1 - self.distr[i])
            if u > max_u:
                result, max_u = i, u
            elif u == max_u and self.distr[result] > self.distr[i]:
                result = i
        return result

class StipToSequence:
    def __init__(self, stip, model):
        """
        :param stip: STIP video matrix
        :param model: trained FrameToCluster model
        """
        self.model = model
        self.seq = []
        self.time = []
        seq_time = stip[:, 2]
        X = stip[:, 3:]
        start, end = 0, 0
        while start < X.shape[0]:
            while end < X.shape[0] and seq_time[end] == seq_time[start]:
                end += 1
            cluster = self.model.get_cluster(X[start:end])
            if cluster != -1:
                self.seq.append(cluster)
                self.time.append(seq_time[start])
            start = end

    def find_similar(self, other, diagonal=False, min_duration=0, min_L=0.9):
        """
        Finds the most similar subsequences
        :param other: fragment we are looking for
        :param diagonal: if True, algorithm takes into account only
        the upper elements of the algorithm matrix
        :param min_duration: minimum number of frames in
        found fragments
        :param min_L: minimum value of the L-ratio
        :return: 
        """
        gap_penalty = 1
        shift_penalty = 1
        match_penalty = 1

        a_size, b_size = len(self.seq), len(other.seq)
        distance = np.zeros((a_size + 1, b_size + 1), dtype=float)
        traceback = [[(0, 0)] * (b_size + 1)]
        max_score = 0

        # Changes for repeating_subseq
        if diagonal:
            colrow = lambda x: x
            inf = -10 * a_size
            for row in range(a_size + 1):
                for column in range(row, b_size + 1):
                    distance[row][column] = inf
        else:
            colrow = lambda x: (b_size + 1)

        # Filling the distance table and traceback
        for row in range(1, a_size + 1):
            traceback_line = [(0, 0)]
            for column in range(1, colrow(row)):
                fee = match_penalty if self.seq[row - 1] == other.seq[column - 1] else -shift_penalty

                first_deletion = distance[row - 1][column] - gap_penalty
                first_traceback = (row - 1, column)

                second_deletion = distance[row][column - 1] - gap_penalty
                second_traceback = (row, column - 1)

                distance[row][column] = max(
                    distance[row - 1][column - 1] + fee,
                    first_deletion,
                    second_deletion,
                    0)

                if distance[row][column] > max_score:
                    max_score = distance[row][column]

                if distance[row][column] == 0:
                    traceback_line.append((0, 0))
                elif distance[row][column] == first_deletion:
                    traceback_line.append(first_traceback)
                elif distance[row][column] == second_deletion:
                    traceback_line.append(second_traceback)
                else:
                    traceback_line.append((row - 1, column - 1))
            traceback.append(traceback_line)

        # Results based on traceback
        def local_max(distance, row, column):
            if row != len(distance) - 1 and distance[row][column] <= distance[row + 1][column]:
                return False
            if column != len(distance[0]) - 1 and distance[row][column] <= distance[row][column + 1]:
                return False
            if row != len(distance) - 1 and column != len(distance[0]) - 1 and \
                            distance[row][column] <= distance[row + 1][column + 1]:
                return False
            return True

        subseqs = []
        for row in range(1, a_size + 1):
            for column in range(1, colrow(row)):
                if not local_max(distance, row, column):
                    continue
                trace_i, trace_j = traceback[row][column]
                a_begin, b_begin = row - 1, column - 1
                while distance[trace_i][trace_j] != 0:
                    a_begin, b_begin = trace_i - 1, trace_j - 1
                    trace_i, trace_j = traceback[trace_i][trace_j]
                seq_size = len(other.seq) if not diagonal else (row - a_begin + 1)
                if seq_size < min_duration:
                    L = 0
                else:
                    L = float(distance[row][column]) / seq_size
                if L < min_L:
                    continue
                subseq = (float(self.time[a_begin]), float(self.time[row]))
                subseqs.append(subseq)

        return subseqs

    def find_repeating(self, min_duration=10, min_L=0.9):
        subseqs = self.find_similar(self, diagonal=True, min_duration=min_duration, min_L=min_L)
        result = set(subseqs)
        return list(result)

class AdvDeletion:
    def __init__(self, video_path, model, stip_folder=None, compress_size=None):
        """
        :param video_path: path to the video file
        :param model: FrameToCluster trained model
        :param stip_folder: optional, path to STIP
        :param compress_size: optional, how to compress the video
        """
        if not stip_folder:
            stip_folder = STIP_FOLDER
        if not compress_size:
            compress_size = COMPRESS_SIZE

        self.video_path = video_path
        capture = cv2.VideoCapture(video_path)

        if not os.path.isdir(STIPPED_VIDEOS_FOLDER):
            os.makedirs(STIPPED_VIDEOS_FOLDER)

        (video_directory, video_filename) = os.path.split(video_path)
        result_path = os.path.join(STIPPED_VIDEOS_FOLDER, video_filename)

        if os.path.isfile(result_path):
            os.remove(result_path)

        width, height = capture.get(3), capture.get(4)
        shrink_width, shrink_height = compress_size
        scale = min(shrink_width / width, shrink_height / height)
        width, height = int(scale * width), int(scale * height)

        print('Compressing video...')
        capture.release()
        subprocess.call('ffmpeg -i {0} -vf scale={1}:{2} {3}'.format(video_path, width, height, result_path),
                        shell=True)

        input_path = os.path.join(STIPPED_VIDEOS_FOLDER, 'stip_list.txt')
        bare_filename, extenstion = os.path.splitext(video_filename)
        with open(input_path, 'w') as stip_list:
            stip_list.write(bare_filename)

        stip_path = os.path.join(stip_folder, 'bin', 'stipdet')
        stip_path = os.path.realpath(stip_path)

        output_path = os.path.join(STIPPED_VIDEOS_FOLDER, 'stip_list.txt')
        output_path = os.path.realpath(output_path)
        input_path = os.path.realpath(input_path)

        print('STIP is now working...')
        subprocess.call('{0} -i {1} -vpath {2}/ -ext {3} -o {4}'.format(stip_path, input_path, STIPPED_VIDEOS_FOLDER, extenstion, output_path),
                        shell=True)

        X = self.read_stip(output_path)
        print('Vectorizer is now working...')
        self.sequence = StipToSequence(X, model)

    def read_stip(self, filename):
        """
        Reads STIP output and generates frame matrix, each
        row of each is y-norm, x-norm, t-norm and HOG-HOF
        description.
        :param filename: file to be read
        :return: frame matrix
        """
        file = open(filename, 'r')
        lines = file.readlines()
        matrix = [line.split() for line in lines[2:-1]]
        matrix = sorted(matrix, key=lambda x: x[3])
        matrix = np.matrix(matrix, dtype=float)
        info = matrix[:, 1:4]
        descriptors = matrix[:, 9:-1]
        return np.concatenate((info, descriptors), axis=1)


    def extractAdv(self):
        """
        Searches for repeating parts
        :return: intervals of repeating parts
        """
        subseqs = self.sequence.find_repeating()
        intervals = covering(subseqs)
        return intervals

    def deleteAdv(self, adv):
        """
        Searches for the advert
        :param adv: StipToSequence advert
        :return: time intervals of the advert
        """
        subseqs = self.sequence.find_similar(adv)
        intervals = covering(subseqs)
        return intervals

class VideoCutter:
    def __init__(self, filename):
        """
        :param filename: the filename of a video
        """
        self.filename = filename
        video = cv2.VideoCapture(self.filename)
        self.duration = video.get(7) / video.get(5)  # number of frames / FPS
        video.release()

    def extract(self, intervals, result_folder):
        """
        Extracts subsequences into a folder
        :param intervals: intervals to be extracted
        :param result_folder: where to save the result
        """
        intervals = covering(intervals)
        if not os.path.isdir(result_folder):
            os.makedirs(result_folder)
        input_path = os.path.realpath(self.filename)
        (_, extension) = os.path.splitext(self.filename)

        for i, interval in enumerate(intervals):
            path = os.path.join(result_folder, 'extracted' + str(i) + extension)
            ss = self.duration * interval[0]
            t = self.duration * (interval[1] - interval[0])
            subprocess.call('ffmpeg -i {0} -ss {1} -t {2} -c copy {3}'.format(input_path, ss, t, path),
                            shell=True)

    def erase(self, intervals, result_filename):
        """
        Erases subsequences from a video sequence
        :param intervals: intervals to be erased
        :param result_filename: where to save the result
        """
        intervals = covering(intervals)
        erase_intervals = []
        start = 0
        for interval in intervals:
            if start == interval[0]:
                start = interval[1]
            else:
                erase_intervals.append((start, interval[1]))
                start = interval[1]
        if start != 1 and intervals[-1][1] != 1:
            erase_intervals.append((start, 1))
        intervals = erase_intervals

        if not os.path.isdir(VIDEOCUTTER_FOLDER):
            os.makedirs(VIDEOCUTTER_FOLDER)
        config_path = os.path.join(VIDEOCUTTER_FOLDER, 'config.txt')
        ffmpeg_config = open(config_path, 'w')
        full_path = os.path.realpath(VIDEOCUTTER_FOLDER)
        input_path = os.path.realpath(self.filename)

        (_, extension) = os.path.splitext(self.filename)

        for i, interval in enumerate(intervals):
            path = os.path.join(full_path, 'temp' + str(i) + extension)
            ffmpeg_config.write('file \'' + path + '\'\n')
            ss = self.duration * interval[0]
            t = self.duration * (interval[1] - interval[0])
            subprocess.call('ffmpeg -i {0} -ss {1} -t {2} -c copy {3}'.format(input_path, ss, t, path),
                            shell=True)

        ffmpeg_config.close()

        subprocess.call('ffmpeg -f concat -safe 0 -i {0} -c copy {1}'.format(os.path.realpath(config_path), result_filename),
                        shell=True)

        for filename in os.listdir(full_path):
            file_path = os.path.join(full_path, filename)
            if os.path.isfile(file_path):
                os.unlink(file_path)


if __name__ == '__main__':
    print('advdeletion library is not expected to be run directly.')
    exit(0)