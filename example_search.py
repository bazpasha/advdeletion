import advdeletion as ad
import pickle

# Loading pickled model
with open('model.pkl', 'r') as model_file:
    model = pickle.load(model_file)

# Setting STIP path
ad.STIP_FOLDER = '/home/bazpasha/Desktop/stip'

# Opening videos
full = ad.AdvDeletion('toy_full.mp4', model)
adv = ad.AdvDeletion('toy_adv.mp4', model)

print('Searching the advertisement...')
intervals = full.deleteAdv(adv.sequence)

# Extracting found advertisement
cutter = ad.VideoCutter('toy_full.mp4')
cutter.extract(intervals, 'example_search_videos')